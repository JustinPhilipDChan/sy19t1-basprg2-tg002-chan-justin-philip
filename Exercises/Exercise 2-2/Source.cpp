#include<iostream>
#include <time.h>

using namespace std;

struct dice
{
	int first = 0, second = 0, value = 0;
};

int wager(int& gold)
{
	int bet;
	cout << "Input bet: ";
	do
	{
		cin >> bet;
		if (bet > gold)
		{
			cout << "Insuffecient gold. Please input another desired bet amount.\t";
		}
	} while (bet > gold); return bet;
}

dice rollDice()
{
	dice roll;
	roll.first = rand() % 6 + 1; roll.second = rand() % 6 + 1;
	roll.value = roll.first + roll.second;
	return roll;
}

void printRoll(dice diceRoll)
{
	cout << "First dice: \t" << diceRoll.first << endl << "Second dice:\t" << diceRoll.second << endl << "Dice value:  \t" << diceRoll.value << endl;
}

int main()
{
	srand((unsigned int)time(NULL));
	dice player, dealer;
	cout << "Player will now roll." << endl;
	player = rollDice(); printRoll(player);
	cout << endl << endl << endl << "Dealer will now roll." << endl;
	dealer = rollDice(); printRoll(dealer);
	system("pause");
	return 0;
}