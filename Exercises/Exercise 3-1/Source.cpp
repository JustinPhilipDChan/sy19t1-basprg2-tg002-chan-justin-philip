#include<iostream>
#include<time.h>

using namespace std;

int randomNumbers(int* Array)
{
	for (int i = 0; i < 10; i++) {
		Array[i] = rand() % 100 + 1;
	}
	return *Array;
}

int main() {

	srand(time(NULL));
	int Array[10];

	randomNumbers(Array);

	for (int i = 0; i < 10; i++) {
		cout << Array[i] << " ";
	}

	system("pause");
	return 0;
}