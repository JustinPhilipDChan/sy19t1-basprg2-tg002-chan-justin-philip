#include<iostream>
#include <time.h>

using namespace std;

struct dice
{
	int first = 0, second = 0, value = 0;
};

dice rollDice()
{
	dice roll;
	roll.first = rand() % 6 + 1; roll.second = rand() % 6 + 1;
	roll.value = roll.first + roll.second;
	return roll;
}

void printRoll(dice diceRoll)
{
	cout << "First dice: \t" << diceRoll.first << endl << "Second dice:\t" << diceRoll.second << endl << "Dice value:  \t" << diceRoll.value << endl;
}

//void bet
void wagerVoid(dice player, dice dealer, int& gold)
{
	int bet;
	cout << "Input bet: ";
	do
	{
		cin >> bet;
		if (bet > gold)
		{
			cout << "Insuffecient gold. Please input another desired bet amount.\t";
		}
	} while (bet > gold);
	if ((player.value == 2) && (dealer.value != 2))
	{
		gold = gold + bet * 3;
	}
	else if ((dealer.value == 2) && (player.value != 2))
	{
		gold = gold + (bet * 3);
	}
	else if (player.value > dealer.value)
	{
		gold = gold + bet;
	}
	else if (player.value < dealer.value)
	{
		gold = gold - bet;
	}
}

//int bet
int wager(dice player, dice dealer,int& gold)
{
	int bet;
	cout << "Input bet: ";
	do
	{
		cin >> bet;
		if (bet > gold)
		{
			cout << "Insuffecient gold. Please input another desired bet amount.\t";
		}
	} while (bet > gold);
	if ((player.value == 2) && (dealer.value != 2))
	{
		gold = gold + bet * 3;
	}
	else if ((dealer.value == 2) && (player.value != 2))
	{
		gold = gold + (bet * 3);
	}
	else if (player.value > dealer.value)
	{
		gold = gold + bet;
	}
	else if (player.value < dealer.value)
	{
		gold = gold - bet;
	}
	return gold;
}

int playRound(int& gold, int& bet)
{
	dice player, dealer;

	do
	{
		cout << "Don't make a bet you can't afford!!!" << endl << endl << "Player's current gold: " << gold << endl;
		bet = wager(player, dealer, gold);
		system("pause"); system("cls");
		cout << "The dealer will now roll..." << endl << endl;
		dealer = rollDice(); printRoll(dealer);
		system("pause"); system("cls");
		cout << "The player will now roll..." << endl << endl;
		system("pause");
		player = rollDice(); printRoll(player);
		system("pause"); system("cls");
		cout << endl << "Dealer:\n"; printRoll(dealer); cout << endl << endl << "Player:\n"; printRoll(player);
		system("pause"); system("cls");
	} while (gold > 0);
	if (gold <= 0)
	{
		cout << "Player ran out of gold. :(" << endl;
	}
	return gold;
}

int main()
{
	srand((unsigned int)time(NULL));
	dice player, dealer;
	int bet, gold = 1000;
	int& betRef = bet;
	int& goldRef = gold;
	playRound(gold, bet);
	system("pause"); return 0;
}