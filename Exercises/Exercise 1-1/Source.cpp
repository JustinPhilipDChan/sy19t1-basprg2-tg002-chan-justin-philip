#include<iostream>

using namespace std;

int factorial(int input, int Fact)
{
	if (input != 0)
	{
		for (int i = 1; i <= input; i++)
		{
			Fact = Fact * i;
		}
	}
	else
	{
		Fact = 1;
	}
	return Fact;
}

int main()
{
	int Fact = 1, input;
	cout << "Input a number: ";
	cin >> input;
	cout << "Factorial of " << input << " is: " << factorial(input, Fact) << endl << endl;
	system("pause");
	return 0;
}