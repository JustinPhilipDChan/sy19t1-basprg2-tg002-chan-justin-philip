#include<iostream>
#include<ctime>
#include<string>
#include<conio.h>

using namespace std;

//EX 3-4
struct Item {
	string name;
	int gold = 0;
};
// function for generating a random item
Item randomItem(string items[])
{
	Item random;
	int i = rand() % 5;
	
	if (i == 0) {
		random.name = "Cursed Stone";
		random.gold = 0;
		cout << "You got a " << items[i] << "!" << endl;
	}
	else if (i == 1) {
		random.name = "Jellopy";
		random.gold = 5;
		cout << "You got a " << items[i] << "!" << endl;
	}
	else if (i == 2) {
		random.name = "Thick Leather";
		random.gold = 25;
		cout << "You got a " << items[i] << "!" << endl;
	}
	else if (i == 3) {
		random.name = "Sharp Talon";
		random.gold = 50;
		cout << "You got a " << items[i] << "!" << endl;
	}
	else if (i == 4) {
		random.name = "Mithril Ore";
		random.gold = 100;
		cout << "You got a " << items[i] << "!" << endl;
	}
	return random;
}

//EX 3-5 function for entering the dungeon
int enterDungeon(int* gold, string items[])
{
	char exit;
	int mult = 1;
	int goldHold = 0;

	*gold -= 25;
	cout << "\n\nYOU ARE NOW INSIDE THE DUNGEON! :O" << endl << endl;
	system("pause");
	do
	{
		Item random = randomItem(items);
		if (random.name == "Cursed Stone") {
			cout << "You got a " << random.name << ", you're automatically kicked from the dungeon." << endl;
			return *gold;
		}
		else if (random.name == "Jellopy") {
			cout << "You earned " << random.gold << "!" << endl << endl;
		}
		else if (random.name == "Thick Leather") {
			cout << "You earned " << random.gold << "!" << endl << endl;
		}
		else if (random.name == "Sharp Talon") {
			cout << "You earned " << random.gold << "!" << endl << endl;
		}
		else if (random.name == "Mithril Ore") {
			cout << "You earned " << random.gold << "!" << endl << endl;
		}
		system("pause");

		goldHold += (random.gold * mult);

		cout << "\n\nTotal Earned gold this run is " << goldHold << " gold" << endl;
		cout << "Current multiplier: x" << mult << endl;

		mult++;
		if (mult >= 4) {
			mult = 4;
		}

		cout << "\nDo you want to continue exploring the dungeon? (Press 'x' to exit)\n";
		exit = _getch();
		system("pause");
		system("cls");
	} while (exit != 'x');
	*gold += goldHold;
	return *gold;
}

//function for the ending
void ending(int* gold)
{
	if (*gold >= 500)
	{
		cout << "WHAT AN ADVENTURER, WILLING TO TAKE RISKS FOR THE BOUNTY." << endl << endl << endl
			<< "Dear adventurer, you have won this mini dungeon game, the creator of this dungeon wishes you well." << endl;
	}
	else if (*gold <= 24)
	{
		cout << "\n\nYou can't pay for the dungeon entrance anymore. Better luck next time, adventurer! :(" << endl;
	}
}

int main() {
	srand(time(NULL));
	string items[5] = { "Cursed Stone", "Jellopy", "Thick Leather", "Sharp Talon", "Mitrhil Ore"};
	int* gold = new int;
	*gold = 50;

	cout << "WELCOME TO THE MINI DUNGEON GAME, ADVENTURER!" << endl;

	//EX 3-6
	while (*gold >= 25 && *gold < 500)
	{
		enterDungeon(gold, items);
		if (*gold <= 24)
		{
			break;
		}
		system("pause");
		system("cls");
	}
	ending(gold);
	system("pause");
	return 0;
}