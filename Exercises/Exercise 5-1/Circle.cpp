#include "Circle.h"

Circle::Circle(float radius)
	:Shape("Circle", 0)
{
	mRadius = radius;
}

Circle::~Circle()
{
}

float Circle::getArea()
{
	return 3.14f* mRadius * mRadius;
}
