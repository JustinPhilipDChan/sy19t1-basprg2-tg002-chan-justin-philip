#pragma once
#include "Shape.h"
class Rectangle :
	public Shape
{
public:
	Rectangle(float length, float width);
	~Rectangle();

	float getArea() override;

private:
	float mLength;
	float mWidth;
};

