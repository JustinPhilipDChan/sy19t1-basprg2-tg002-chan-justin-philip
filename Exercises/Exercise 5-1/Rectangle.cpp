#include "Rectangle.h"

Rectangle::Rectangle(float length, float width)
	:Shape("Rectangle", 4)
{
	mLength = length;
	mWidth = width;
}

Rectangle::~Rectangle()
{
}

float Rectangle::getArea()
{
	return mLength*mWidth;
}
