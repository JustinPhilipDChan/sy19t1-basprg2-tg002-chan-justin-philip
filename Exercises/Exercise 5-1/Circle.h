#pragma once
#include "Shape.h"
class Circle :
	public Shape
{
public:
	Circle(float radius);
	~Circle();

	float getArea() override;

private:
	float mRadius;
};

