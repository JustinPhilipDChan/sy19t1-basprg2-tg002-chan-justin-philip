#include <iostream>
#include <string>
#include <vector>
#include "Circle.h"
#include "Rectangle.h"
#include "Square.h"
#include "Shape.h"

using namespace std;

void printShape(const vector<Shape*>& shapes)
{
	for (int i = 0; i < shapes.size(); i++)
	{
		Shape* shape = shapes[i];
		cout << shape->getName() << ":\nArea: " << shape->getArea()<< "\nNumber of sides: " << shape->getNumSides() << endl << endl;
	}
}

int main()
{
	vector<Shape*> shapes;
	
	Square* square = new Square(5);
	shapes.push_back(square);

	Circle* circle = new Circle(10);
	shapes.push_back(circle);

	Rectangle* rectangle = new Rectangle(3, 10);
	shapes.push_back(rectangle);

	printShape(shapes);

	delete square;
	delete circle;
	delete rectangle;

	system("pause");
	return 0;
}