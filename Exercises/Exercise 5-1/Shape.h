#include <string>
using namespace std;

#pragma once

class Shape 
{
public:
	Shape(string name, int numSides);
	~Shape();

	string getName();
	int getNumSides();
	virtual float getArea() = 0;

private:
	string mName;
	int mNumSides;
};