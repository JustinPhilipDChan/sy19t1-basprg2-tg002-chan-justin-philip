#include "Square.h"

Square::Square(float length)
	:Shape("Square", 4)
{
	mLength = length;
}

Square::~Square()
{
}

float Square::getArea()
{
	return mLength*mLength;
}
