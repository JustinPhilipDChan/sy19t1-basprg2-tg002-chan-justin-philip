#include "Shape.h"

Shape::Shape(string name, int numSides)
{
	mName = name;
	mNumSides = numSides;
}

Shape::~Shape()
{
}

string Shape::getName()
{
	return mName;
}

int Shape::getNumSides()
{
	return mNumSides;
}
