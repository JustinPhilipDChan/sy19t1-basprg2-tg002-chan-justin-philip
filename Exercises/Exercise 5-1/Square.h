#pragma once
#include "Shape.h"
class Square :
	public Shape
{
public:
	Square(float length);
	~Square();

	float getArea() override;

private:
	float mLength;
};

