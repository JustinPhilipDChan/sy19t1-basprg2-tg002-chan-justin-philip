#include<iostream>
#include<string>

using namespace std;

void printArray(string items[])
{
	for (int i = 0; i < 8; i++)
	{
		if (i == 7)
		{
			cout << items[i];
		}
		else
		{
			cout << items[i] << ", ";
		}
	}
}

int main()
{
	string items[] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };
	printArray(items);
	cout << endl << endl;
	system("pause");
	return 0;
}