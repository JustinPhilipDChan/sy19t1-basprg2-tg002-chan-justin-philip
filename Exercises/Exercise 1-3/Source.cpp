#include<iostream>
#include<string>

using namespace std;

void count(string items[])
{
	int counter = 0;
	string input;
	cout << "Input the name of an item in your inventory: ";
	getline(cin, input);
	for (int i = 0; i < 9; i++)
	{
		if (items[i] == input)
		{
			counter++;
		}
	}
	cout << endl << "You have exactly " << counter << " " << input << endl << endl;
}

void printArray(string items[])
{
	for (int i = 0; i < 8; i++)
	{
		if (i == 7)
		{
			cout << items[i];
		}
		else
		{
			cout << items[i] << ", ";
		}
	}
}

int main()
{
	string items[] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };
	cout << "Inventory: ";
	printArray(items);
	cout << "." << endl << endl;
	count(items);
	system("pause");
	return 0;
}