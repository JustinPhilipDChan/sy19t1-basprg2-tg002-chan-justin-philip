#include<iostream>
#include <stdlib.h>
using namespace std;

int* sortingFunction(int numbers[])
{
	int small;
	for (int l = 0; l < 10; l++)
	{
		for (int k = l + 1; k < 10; k++)
		{
			if (numbers[k] < numbers[l])
			{
				small = numbers[l];
				numbers[l] = numbers[k];
				numbers[k] = small;
			}
		}
		cout << numbers[l] << " ";
	}
	cout << endl;
	return numbers;
}

int main()
{
	int numbers[10], INPUT;
	int small;

	cout << "Enter 10 random numbers: ";
	for (int i = 0; i < 10; i++)
	{
		cin >> INPUT;
		numbers[i] = INPUT;
	}
	system("cls");

	cout << "User generated array: ";
	for (int j = 0; j < 10; j++)
	{
		cout << numbers[j] << " ";
	}
	cout << endl << endl;
	sortingFunction(numbers);
	system("pause");
	return 0;
}