#include<iostream>
#include <time.h>

using namespace std;

struct dice
{
	int first = 0, second = 0, value = 0;
};

int wager(int& gold)
{
	int bet;
	cout << "Input bet: ";
	do
	{
		cin >> bet;
		if (bet > gold)
		{
			cout << "Insuffecient gold. Please input another desired bet amount.\t";
		}
	} while (bet > gold); return bet;
}

dice rollDice()
{
	dice roll;
	roll.first = rand() % 6 + 1; roll.second = rand() % 6 + 1;
	roll.value = roll.first + roll.second;
	return roll;
}

void printRoll(dice diceRoll)
{
	cout << "First dice: \t" << diceRoll.first << endl << "Second dice:\t" << diceRoll.second << endl << "Dice value:  \t" << diceRoll.value << endl;
}

int payout(dice player, dice dealer, int bet, int& gold)
{
	if ((player.value == 2) && (dealer.value != 2))
	{
		gold = gold + bet * 3;
	}
	else if ((dealer.value == 2) && (player.value != 2))
	{
		gold = gold + (bet * 3);
	}
	else if (player.value > dealer.value)
	{
		gold = gold + bet;
	}
	else if (player.value < dealer.value)
	{
		gold = gold - bet;
	}
	return gold;
}

int main()
{
	srand((unsigned int)time(NULL));
	int bet, gold = 1000;
	int& betRef = bet;
	int& goldRef = gold;
	dice player, dealer;
	cout << "Don't make a bet you can't afford!!!" << endl << endl << "Player's current gold: " << gold << endl;
	bet = wager(gold);
	system("pause"); system("cls");
	cout << "Player will now roll." << endl;
	player = rollDice(); printRoll(player);
	cout << endl << endl << endl << "Dealer will now roll." << endl;
	dealer = rollDice(); printRoll(dealer);
	payout(player, dealer, bet, gold);
	cout << endl << endl << "Player gold: " << goldRef << endl;
	system("pause");
	return 0;
}