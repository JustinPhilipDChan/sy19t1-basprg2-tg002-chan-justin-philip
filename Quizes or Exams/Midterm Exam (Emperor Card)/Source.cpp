#include<iostream>
#include<stdbool.h>
#include<stdlib.h>
#include<time.h>

using namespace std;

#ifndef NODE_H
#define NODE_H

#include <string>

struct Node
{
	std::string name;
	Node* next = NULL;
};

#endif
//minimum is 1mm and max is 30 mm
int wager(int bet) {
	cout << "How many millimeters are you willing to bet? ";
	do
	{
		cin >> bet;

	} while ((bet > 30) || (bet <= 0));
	system("pause"); system("cls");
	return bet;
}

void UI(int* money, int* mm, string organ, bool Emperor) {
	cout << "Money: " << *money << " Yen\n"
		<< "Distance left: " << *mm << " (mm)\n"
		<< "Kaiji's ultimate wager: " << organ << "\n";
	if (Emperor == true)
		cout << "Side: Emperor\n\n";
	else
		cout << "Side: Slave\n\n";
}

//generate 1 special card (emperor or slave) and 4 civilian cards
void generateNode(Node*& head, Node*& tail, string name) {
	Node* temp = new Node;
	temp->next = NULL;
	if (head == NULL)
	{
		head = temp;
		temp->name = "Special";
		tail = temp;
	}
	for (int i = 0; i < 4; i++)
	{
		Node* hold = new Node;
		hold->name = "Civilian";
		hold->next = head;
		tail->next = hold;
		tail = hold;
	}
	system("pause"); system("cls");
}

//payout
int payout(int pCard, int eCard, bool Emperor, int bet, int* money, int* mm) {
	if ((pCard == 0) && (eCard != 0)) //both civilian
	{
		cout << "\nBOTH PLAYERS CHOSE CIVILIAN.\n\n";
		return *mm;
	}
	else if ((pCard == 1) && (eCard == 0))
	{
		if (Emperor == true) //player emperor while enemy slave
		{
			cout << "\nKaiji lost " << bet << " mm.\n\n";
			*mm -= bet;
			return *mm;
		}
		else if (Emperor == false)
		{
			bet *= 500000;
			*money += bet;
			cout << "\nKaiji won " << bet << " Yen.\n\n";
			return *money;
		}
	}
	else if ((pCard == 1) && (eCard != 0))
	{
		if (Emperor == true) //player emperor while enemy civilian
		{
			bet *= 100000;
			*money += bet;
			cout << "\nKaiji won " << bet << " Yen.\n\n";
			return *money;
		}
		else if (Emperor == false)
		{
			cout << "\nKaiji lost " << bet << " mm.\n\n";
			*mm -= bet;
			return *mm;
		}
	}
	else if ((pCard == 0) && (eCard == 0))
	{
		if (Emperor == true) //player civilian while enemy emperor
		{
			cout << "\nKaiji lost " << bet << " mm.\n\n";
			*mm -= bet;
			return *mm;
		}
		else if (Emperor == false) // player civilian while enemy slave
		{
			bet *= 500000;
			*money += bet;
			cout << "\nKaiji won " << bet << " Yen.\n\n";
			return *money;
		}
	}
	return *mm;
}

void displayList(Node* head, bool Emperor) {
	cout << "Kaiji currently has: ";
	Node* C = head;

	if (Emperor == true)
		head->name = "Emperor";
	else
		head->name = "Slave";
	do
	{
		cout << C->name << ' ';
		C = C->next;
	} while (C != head);

	cout << endl << endl << endl;
}

//DELETE
void deleteNode(Node*& head, Node*& tail) {
	Node* B = head;
	Node* C = head;
	//always deletes the last node which is the civilian node
	if (head == tail)
	{
		delete head;
		head = NULL;
		tail = NULL;
	}
	else
	{
		//move head to last
		while (C->next != head)
		{
			B = C;
			C = C->next;
		}
		B->next = C->next;
		delete C;
		C = NULL;
	}
}

//play round
int playRound(Node*& head, Node*& tail, bool Emperor, int bet, int* money, int* mm) {
	int pCard, eCard; //enemy and player card
	int size = 4;
	for (int i = 0; i < 5; i++) // PLAYS 5 ROUNDS. EXITS IF PLAYER OR ENEMY USES HIS SPECIAL CARD
	{
		eCard = rand() % size; // AI chooses slave or civilian

		if (Emperor == true)	//display what side Kaiji's at
			cout << "Kaiji has the Emperor card!\n\n";
		else if (Emperor == false)
			cout << "Kaiji has the Slave card!\n\n";

		displayList(head, Emperor); //DISPLAY KAIJI'S CARDS

		cout << "\nWhich card will you pick? (Press 0 for a civilian card, press 1 for the Emperor/Slave card) ";
		cin >> pCard;
		system("pause"); system("cls");

		if (Emperor == true)
			cout << "Kaiji has the Emperor card!\n\n\n";
		else if (Emperor == false)
			cout << "Kaiji has the Slave card!\n\n\n";

		if (eCard == 0)
		{
			if (Emperor == true)
				cout << "Yukio Tonegawa chose the [Slave] card\n\n";
			else
				cout << "Yukio Tonegawa chose the [Emperor] card\n\n";
		}
		else if (eCard != 0)
		{
			cout << "Yukio Tonegawa chose a [Civilian] card\n\n";
		}
		//display player card
		if (pCard == 0)
		{
			cout << "Kaiji chose a [Civilian] card\n\n";
			if ((Emperor == true) && (eCard == 0)) //player civilian wins against slave
			{
				payout(pCard, eCard, Emperor, bet, money, mm);
				break;
			}
			else if (eCard != 0)   //both same so minus civilian
			{
				deleteNode(head, tail);
			}
			else if ((Emperor == false) && (eCard == 0))
			{
				payout(pCard, eCard, Emperor, bet, money, mm);
				break;
			}
		}
		else if (pCard == 1)
		{
			if ((Emperor == true) && (eCard != 0))		//player SPECIAL CARD and enemy != special card
			{
				cout << "Kaiji chose the [Emperor] card\n\n";
				payout(pCard, eCard, Emperor, bet, money, mm);
				break;
			}
			else if ((Emperor == false) && (eCard == 0))	//player SLAVE and enemy EMPEROR
			{
				cout << "Kaiji chose the [Slave] card\n\n";
				payout(pCard, eCard, Emperor, bet, money, mm);
				break;
			}
			else if ((Emperor == false) && (eCard != 0))	//player slave and enemy civilian (lost)
			{
				cout << "Kaiji chose the [Slave] card\n\n";
				payout(pCard, eCard, Emperor, bet, money, mm);
				break;
			}
			else if ((Emperor == true) && (eCard == 0))		//player EMPEROR and enemy slave
			{
				cout << "Kaiji chose the [Emperor] card\n\n";
				payout(pCard, eCard, Emperor, bet, money, mm);
				break;
			}
		}
		system("pause"); system("cls");
		size--;
	}
	return *mm;
}

//ending
void evaluateEnding(int* mm, int* money, string organ) {
	system("pause");
	system("cls");
	if ((*mm > 0) && (*money >= 20000000))  //BEST ENDING
	{
		cout << "Congratulations!!!\nKaiji won the Emperor Card Game and Yukio Tonegawa is now doing a dogeza in front of you while apologizing!\n";
	}
	else if ((*mm > 0) && (*money < 20000000))  //MEH ENDING
	{
		cout << "Kaiji has finished the Emperor Card Game but he only has " << *money << " Yen."
			<< "\n\nWhat kind of game will Kaiji play next?\n";
	}
	else if (*mm < 0)
	{
		cout << "Kaiji's " << organ << " has been destroyed! :O\n"
			<< "Try again next time!\n";
	}
}

int main()
{
	srand(time(NULL));
	int bet = 0;
	int* money = new int;
	*money = 0;
	int* mm = new int;
	*mm = 30;
	string organ;
	string name;
	//indicates that the list is empty
	Node* head = NULL;
	Node* tail = NULL;
	bool Emperor = true;
	int round = 1;
	cout << "Which organ do you want to bet? (eye or ear) ";
	cin >> organ; cout << endl;
	system("pause"); system("cls");
	do
	{
		for (int i = 1; i <= 4; i++) // runs 3 times
		{
			if (round % 2 == 1) //EMPEROR ROUNDS
			{
				for (int i = 0; i < 3; i++) // 3 emperor rounds
				{
					UI(money, mm, organ, Emperor);
					generateNode(head, tail, name);
					Emperor = true;
					bet = wager(bet);
					playRound(head, tail, Emperor, bet, money, mm);
					UI(money, mm, organ, Emperor);
					system("pause"); system("cls");
				}
			}
			else if (round % 2 == 0) //SLAVE ROUNDS
			{
				for (int i = 0; i < 3; i++) // 3 slave rounds
				{
					UI(money, mm, organ, Emperor);
					generateNode(head, tail, name);
					Emperor = false;
					bet = wager(bet);
					playRound(head, tail, Emperor, bet, money, mm);
					UI(money, mm, organ, Emperor);
					system("pause"); system("cls");
				}
			}
			round++;
		}
	} while ((*mm < 0) || (round == 4));
	evaluateEnding(mm, money, organ);
	system("pause");
	return 0;
}