#include<iostream>
#include<stdlib.h>
#include<conio.h>

using namespace std;

void goldOutput(int gold, int gem)
{
	cout << "You currently have " << gold << " gold and " << gem << " gems.\n\n";
}

void IAPoutput()
{
	cout << "---------------------------------------------------------\n" << "| Package Name\t| Cost    \t| Hard Currency (Gold)\t|\n" << "---------------------------------------------------------\n";
	cout << "| Bag of Gold\t| $0.99 \t| 1000  \t\t|\n" << "--------------------------------------------------------\n";
	cout << "| Bags of Gold\t| $2.99 \t| 5000  \t\t|\n" << "---------------------------------------------------------\n";
	cout << "| Stash of Gold\t| $9.99 \t| 30000 \t\t|\n" << "---------------------------------------------------------\n";
	cout << "| Gold Vault\t| $49.99\t| 200000\t\t|\n" << "---------------------------------------------------------\n";
} // DISPLAY WHEN U ASK USER FOR IAP

int* packageSorter(int packages[])
{
	int small = 0;
	for (int l = 0; l < 7; l++)
	{
		for (int k = l + 1; k < 7; k++)
		{
			if (packages[k] < packages[l])
			{
				small = packages[l];
				packages[l] = packages[k];
				packages[k] = small;
			}
		}
	}
	return packages;
}

void packageDisplay()
{
	cout << "---------------------------------\n" << "| Packages\t| Gold   \t|\n" << "---------------------------------\n";
	cout << "| 1       \t| 150    \t|\n" << "---------------------------------\n";
	cout << "| 2       \t| 500    \t|\n" << "---------------------------------\n";
	cout << "| 3       \t| 1780   \t|\n" << "---------------------------------\n";
	cout << "| 4       \t| 4050   \t|\n" << "---------------------------------\n";
	cout << "| 5       \t| 13333  \t|\n" << "---------------------------------\n";
	cout << "| 6       \t| 30750  \t|\n" << "---------------------------------\n";
	cout << "| 7       \t| 250000 \t|\n" << "---------------------------------\n";
}

int IAPpayout(int packages[], int gold, int gem, int input)
{
	string x;
	if (input == 500)
	{
		cout << "\nInsuffecient gold.\n";
		system("pause");
		system("cls");
		goldOutput(gold, gem);
		IAPoutput();
		cout << "\n\nWould you like to purchase a Bag of Gold to for $0.99? (Y/N)   ";
		cin >> x;
		if ((x == "Y") || (x == "y"))
		{
			gold = 1000 + gold;
			return gold;
		}
		else
		{
			return gold;
		}
	}
	else if ((input == 1780) || (input == 4050))
	{
		cout << "\nInsuffecient gold.\n";
		system("pause");
		system("cls");
		goldOutput(gold, gem);
		IAPoutput();
		cout << "\n\nWould you like to purchase Bags of Gold to for $2.99? (Y/N)   ";
		cin >> x;
		if ((x == "Y") || (x == "y"))
		{
			gold = 5000 + gold;
			return gold;
		}
		else
		{
			return gold;
		}
	}
	else if (input == 13333)
	{
		cout << "\nInsuffecient gold.\n";
		system("pause");
		system("cls");
		goldOutput(gold, gem);
		IAPoutput();
		cout << "\n\nWould you like to purchase a Stash of Gold to for $9.99? (Y/N)   ";
		cin >> x;
		if ((x == "Y") || (x == "y"))
		{
			gold = 30000 + gold;
			return gold;
		}
		else
		{
			return gold;
		}
	}
	else if (input == 30750)
	{
		cout << "\nInsuffecient gold.\n";
		system("pause");
		system("cls");
		goldOutput(gold, gem);
		IAPoutput();
		cout << "\n\nWould you like to purchase THE Gold Vault to for $49.99? (Y/N)   ";
		cin >> x;
		if ((x == "Y") || (x == "y"))
		{
			gold = 200000 + gold;
			return gold;
		}
		else
		{
			return gold;
		}
	}
	else
	{
		cout << "\nThere's no available type of Gold Packages for your desired item.";
		return gold;
	}
	return gold;
}

int payout(int packages[], int gold, int gem, int input)
{
	if (input == 150)
	{
		cout << "\n\nThank you for purchasing package #1!";
		gold = gold - input;
		return gold;
	}
	else
	{
		IAPpayout(packages, gold, gem, input);
		return gold;
	}
	return gold;
}

int main()
{
	char exitInput;
	int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	int gold = 250, gem = 0, input;
	do
	{
		packageSorter(packages);
		packageDisplay();
		cout << endl << endl;
		goldOutput(gold, gem);
		cout << "Input the price of the item you want to buy: ";
		cin >> input;
		payout(packages, gold, gem, input);
		cout << endl << endl;
		system("pause");
		system("cls");
		cout << "Continue shopping? (Y) yes (N) no";
		exitInput = _getch();
		cout << endl << endl;
	} while (exitInput != 'n');
	system("pause");
	return 0;
}