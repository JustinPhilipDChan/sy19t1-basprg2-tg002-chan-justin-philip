#include<iostream>

using namespace std;

#pragma once

struct Stats
{
	int hp = 0;
	int crystals = 0;
	int rarityPoints = 0;
};

void addStats(Stats& source, const Stats& toAdd);

