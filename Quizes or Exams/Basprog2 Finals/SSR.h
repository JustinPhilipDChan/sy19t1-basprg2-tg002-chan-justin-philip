#pragma once
#include "Item.h"
#include "Unit.h"

class Unit;

class SSR :
	public Item
{
public:
	SSR();
	~SSR();

	void activate(Unit* target) override;
};

