#include "Unit.h"
#include "Item.h"
#include "Stats.h"
#include "Bomb.h"
#include "Crystal.h"
#include "HealthPotion.h"
#include "Rarity.h"
#include "SR.h"
#include "SSR.h"
#include <iostream>
#include <ctime>
#include <string>
#include <cstdlib>

using namespace std;

//ending couts
void zeroHp(); //no hp
void zeroCrystals(); // no crystals
void rarityMax(); // 100 rarity points

//Design cout
void horizontalBar();

//Print
void threePauses();
void printItems(Unit* unit);
void printCollateItems(int ssrC, int srC, int rC, int hpC, int bmbC, int cC);


int main()
{
	srand(time(NULL));
	int pull = 0;
	string name;
	//counters
	int ssrC = 0, srC = 0, rC = 0, hpC = 0, bmbC = 0, cC = 0;

	Stats stats;
	stats.hp = 100;
	stats.crystals = 100;
	stats.rarityPoints = 0;

	horizontalBar();
	cout << "Insert your character name: ";
	cin >> name;
	horizontalBar();
	system("cls");
	Unit* unit = new Unit(name, stats);

	while (true)
	{
		//Display stats
		horizontalBar();
		unit->displayStatus();
		cout << "Pulls: " << pull << endl;
		horizontalBar();
		cout << "(press any key three times as you pull out an item in the gacha)" << endl;
		unit->minusCrystal();

		threePauses();

		//not dynamic weight probability
		int randomizer = rand() % 100 + 1;

		//SSR
		if (randomizer == 1)
		{
			Item* ssr = new SSR();
			unit->addItem(ssr);
			ssr->activate(unit);
			ssrC++;
		}
		//SR
		else if ((randomizer >= 2) && (randomizer <= 10))
		{
			Item* sr = new SR();
			unit->addItem(sr);
			sr->activate(unit);
			srC++;
		}
		//R
		else if ((randomizer >= 11) && (randomizer <= 50))
		{
			Item* r = new Rarity();
			unit->addItem(r);
			r->activate(unit);
			rC++;
		}
		//Health potion
		else if ((randomizer >= 51) && (randomizer <= 65))
		{
			Item* hpPotion = new HealthPotion();
			unit->addItem(hpPotion);
			hpPotion->activate(unit);
			hpC++;
		}
		//Bomb
		else if ((randomizer >= 66) && (randomizer <= 85))
		{
			Item* bomb = new Bomb();
			unit->addItem(bomb);
			bomb->activate(unit);
			bmbC++;
		}
		//Crsytals
		else if ((randomizer >= 86) && (randomizer <= 100))
		{
			Item* crystal = new Crystal();
			unit->addItem(crystal);
			crystal->activate(unit);
			cC++;
		}
		
		pull++;
		system("pause");
		system("cls");

		//ending evaluation
		if (unit->getCurrentHp() <= 0) { //NO HP
			horizontalBar();
			unit->displayStatus();
			cout << "Pulls: " << pull << endl;
			horizontalBar();
			zeroHp();
			break;
		}
		else if (unit->getCrystals() <= 0) { //NO CRYSTALS
			horizontalBar();
			unit->displayStatus();
			cout << "Pulls: " << pull << endl;
			horizontalBar();
			zeroCrystals();
			break;
		}
		else if (unit->getRarityPoints() >= 100) { //RARITY POINTS 100
			horizontalBar();
			unit->displayStatus();
			cout << "Pulls: " << pull << endl;
			horizontalBar(); 
			rarityMax();
			break;
		}
	}
	
	//print collate items
	printCollateItems(ssrC, srC, rC, hpC, bmbC, cC);
	cout << endl;
	horizontalBar();
	printItems(unit); //print all the items including duplicates
	horizontalBar();
	
	delete unit;
	system("pause");
	return 0;
}

void zeroHp()
{
	system("pause");
	system("cls");
	horizontalBar();
	cout << "You lost all your hp, better luck next time!" << endl;
	horizontalBar();
}

void zeroCrystals()
{
	system("pause");
	system("cls");
	horizontalBar();
	cout << "You lost all your crystals, better luck next time!" << endl;
	horizontalBar();
}

void rarityMax()
{
	system("pause");
	system("cls");
	horizontalBar();
	cout << "Congratulations! You achieved the maximum Rarity Points and won this gacha game!" << endl;
	horizontalBar();
}

void horizontalBar()
{
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
}

void threePauses()
{
	cout << "." << endl;
	system("pause");
	cout << "." << endl;
	system("pause");
	cout << "." << endl;
	system("pause");
	cout << endl << endl;
}

void printItems(Unit* unit)
{
	for (int i = 0; i < unit->getItems().size(); i++)
	{
		cout << unit->getItems()[i]->getName() << ", ";
	}
	cout << endl << endl;
}

void printCollateItems(int ssrC, int srC, int rC, int hpC, int bmbC, int cC)
{
	cout << endl << "R x" << rC << endl
		<< "SR x" << srC << endl
		<< "SSR x" << ssrC << endl
		<< "Health Potion x" << hpC << endl
		<< "Bomb x" << bmbC << endl
		<< "Crystals x" << cC << endl << endl;
	horizontalBar();
}
