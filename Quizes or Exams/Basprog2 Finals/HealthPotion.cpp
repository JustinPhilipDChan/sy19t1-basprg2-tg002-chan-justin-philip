#include "HealthPotion.h"

HealthPotion::HealthPotion()
		:Item("hpPotion")
{
	mHeal = 30;
}

HealthPotion::~HealthPotion()
{
}

void HealthPotion::activate(Unit* target)
{
	Item::activate(target);

	target->heal();
	cout << target->getName() << " pulled a Health Potion" << endl;
	cout << target->getName() << " is healed for 30hp!" << endl;
}
