#pragma once
#include "Item.h"
#include "Unit.h"

class Unit;

class Rarity :
	public Item
{
public:
	Rarity();
	~Rarity();
	void activate(Unit* target) override;
};

