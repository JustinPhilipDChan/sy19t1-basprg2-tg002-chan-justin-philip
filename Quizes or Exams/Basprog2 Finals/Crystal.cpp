#include "Crystal.h"

Crystal::Crystal()
	:Item("crystal")
{
}

Crystal::~Crystal()
{
}

void Crystal::activate(Unit* target)
{
	Item::activate(target);

	target->crystalGain();
	cout << target->getName() << " pulled a CRYSTAL" << endl;
	cout << target->getName() << " got 15 crystals" << endl;
}
