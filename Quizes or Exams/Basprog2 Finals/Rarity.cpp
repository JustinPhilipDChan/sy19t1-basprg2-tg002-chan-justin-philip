#include "Rarity.h"

Rarity::Rarity()
	:Item("R")
{
}

Rarity::~Rarity()
{
}

void Rarity::activate(Unit* target)
{
	Item::activate(target);

	target->rarityGain(1);
	cout << target->getName() << " pulled out a R item!" << endl;
}
