#pragma once
#include "Item.h"
#include "Unit.h"

class Unit;

class Bomb :
	public Item
{
public:
	Bomb();
	~Bomb();

	void activate(Unit* target) override;

private:
};

