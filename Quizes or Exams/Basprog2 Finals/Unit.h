#include <string>
#include "Stats.h"
#include <vector>
#include "Item.h"

class Item;

using namespace std;



#pragma once
class Unit
{
public:
	Unit(string name, const Stats& stats);
	~Unit();

	//Getters
	string getName();
	Stats& getStats();
	int getCrystals();
	int getRarityPoints();
	int getCurrentHp();

	const vector<Item*> getItems();

	void addItem(Item* item);

	//pang add sa stats para mas madali
	void heal();
	void damage();
	void crystalGain();
	void rarityGain(int points);

	//minus crystal per round
	void minusCrystal();

	bool alive();
	void displayStatus();
	
private:
	string mName;
	Stats mStats;
	int mCurrentHp;
	vector<Item*> mItems;
};

