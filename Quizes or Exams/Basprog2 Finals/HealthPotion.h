#pragma once
#include "Item.h"
#include "Unit.h"

class Unit;

class HealthPotion :
	public Item
{
public:
	HealthPotion();
	~HealthPotion();

	void activate(Unit*target) override;

private:
	int mHeal;
};

