#include "SR.h"

SR::SR()
:Item("SR")
{
}

SR::~SR()
{
}

void SR::activate(Unit* target)
{
	Item::activate(target);

	target->rarityGain(10);
	cout << target->getName() << " got a SR item!!! OwO" << endl;
}
