#include "Stats.h"

void addStats(Stats& source, const Stats& toAdd)
{
	source.hp += toAdd.hp;
	source.crystals += toAdd.crystals;
	source.rarityPoints += toAdd.rarityPoints;
}
