#include "Unit.h"

Unit::Unit(string name, const Stats& stats)
{
	mName = name;
	mStats = stats;

	mCurrentHp = 100;
}

Unit::~Unit()
{
	//Delete all items
	for (int i = 0; i < mItems.size(); i++)
	{
		delete mItems[i];
	}
}

string Unit::getName()
{
	return mName;
}

Stats& Unit::getStats()
{
	return mStats;
}

int Unit::getCrystals()
{
	return mStats.crystals;
}

int Unit::getRarityPoints()
{
	return mStats.rarityPoints;
}

int Unit::getCurrentHp()
{
	return mCurrentHp;
}

const vector<Item*> Unit::getItems()
{
	return mItems;
}

void Unit::addItem(Item* item)
{
	item->setActor(this);

	mItems.push_back(item);
}

void Unit::heal()
{
	mCurrentHp += 30;

	// Clamp hp to max hp
	if (mCurrentHp > mStats.hp) mCurrentHp = mStats.hp;
}

void Unit::damage()
{
	mCurrentHp -= 25;

	//if hp becomes negative, it goes back to zero
	if (mCurrentHp < 0) mCurrentHp = 0;
}

void Unit::crystalGain()
{
	mStats.crystals += 15;
}

void Unit::rarityGain(int points)
{
	mStats.rarityPoints += points;
}

void Unit::minusCrystal()
{
	mStats.crystals -= 5;

	//locks crystals to 0
	if (mStats.crystals < 0) mStats.crystals = 0;
}

bool Unit::alive()
{
	return mCurrentHp < 0;
}

void Unit::displayStatus()
{
	cout << "Name: " << mName << endl
		<< "HP: " << mCurrentHp << "/" << mStats.hp << endl
		<< "Crystals: " << mStats.crystals << endl
		<< "Rarity Points: " << mStats.rarityPoints << endl;
}
