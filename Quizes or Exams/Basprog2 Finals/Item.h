#include <string>
#include <iostream>

using namespace std;

class Unit;

#pragma once
class Item
{
public:
	Item(string name);
	~Item();

	//getter
	string getName();
	Unit* getActor();
	//setter

	void setActor(Unit* actor);
	virtual void activate(Unit* target);

private:
	string mName;
	Unit* mActor;
};

