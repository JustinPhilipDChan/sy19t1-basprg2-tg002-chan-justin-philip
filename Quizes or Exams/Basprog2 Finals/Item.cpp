#include "Item.h"

Item::Item(string name)
{
	mName = name;
}

Item::~Item()
{
}

string Item::getName()
{
	return mName;
}

Unit* Item::getActor()
{
	return mActor;
}

void Item::setActor(Unit* actor)
{
	mActor = actor;
}

void Item::activate(Unit* target)
{
	if (mActor == NULL || target == NULL) throw exception("Actor or target cannot be null");
}
