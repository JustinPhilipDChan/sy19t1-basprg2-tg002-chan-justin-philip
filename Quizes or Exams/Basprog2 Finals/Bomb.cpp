#include "Bomb.h"

Bomb::Bomb()
	:Item("bomb")
{
}

Bomb::~Bomb()
{
}

void Bomb::activate(Unit* target)
{
	Item::activate(target);

	target->damage();
	cout << target->getName() << " pulled a BOMB!" << endl;
	cout << "OUCH!!!! " << target->getName() << " took 25 damage." << endl;
}
