#pragma once
#include "Item.h"
#include "Unit.h"

class Unit;

class Crystal :
	public Item
{
public:
	Crystal();
	~Crystal();

	void activate(Unit* target) override;

private:

};

