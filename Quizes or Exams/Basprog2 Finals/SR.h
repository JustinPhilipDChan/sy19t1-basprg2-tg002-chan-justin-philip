#pragma once
#include "Item.h"
#include "Unit.h"

class Unit;

class SR :
	public Item
{
public:
	SR();
	~SR();

	void activate(Unit* target) override;
};

