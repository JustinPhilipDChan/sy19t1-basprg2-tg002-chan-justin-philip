#include "Monster.h"
#include "Player.h"
#include<iostream>

class Player;

Monster::Monster()
{
	mName = "Monster";
	mRandom = rand() % 2;
	if (mRandom == 0) mMonsterClass = "Warrior";
	else if (mRandom == 1) mMonsterClass = "Assassin";
	else if (mRandom == 2) mMonsterClass = "Mage";
	mHp = (rand() % 15) + 10;
	mBaseHp = mHp;
	mPow = 5;
	mVit = 5;
	mDex = 5;
	mAgi = 5;
	
}

Monster::Monster(string name, string monsterClass, int hp, int baseHp, int pow, int vit, int dex, int agi, int random)
{
	mName = name;
	mMonsterClass = monsterClass;
	mHp = hp;
	mBaseHp = baseHp;
	mPow = pow;
	mVit = vit;
	mDex = dex;
	mAgi = agi;

	mRandom = random; // not sure if this has any use, i highly doubt it does
}

Monster::~Monster() //deconstructor
{
}

int Monster::getHp()
{
	return mHp;
}

int Monster::getBaseHp()
{
	return mBaseHp;
}

int Monster::getPow()
{
	return mPow;
}

int Monster::getVit()
{
	return mVit;
}

int Monster::getDex()
{
	return mDex;
}

int Monster::getAgi()
{
	return mAgi;
}

void Monster::takeDamage(int damage)
{
	if (damage < 0) return;

	mHp -= damage;
	if (mHp < 0) mHp = 0;
}

void Monster::mJobStats()
{
	if (mMonsterClass == "Warrior")
	{
		mHp = mHp + ((rand() % 3 + 1) + 4);
		mBaseHp = mHp;
		mVit = mVit + ((rand() % 2 + 1) + 2);
	}
	else if (mMonsterClass == "Assassin")
	{
		mPow = mPow + ((rand() % 2 + 1) + 2);
		mAgi = mAgi + ((rand() % 3 + 1) + 2);
	}
	else if (mMonsterClass == "Mage")
	{
		mPow = mPow + ((rand() % 3 + 1) + 3);
		mDex = mDex + ((rand() % 2 + 1) + 2);
	}
}

void Monster::mAttack(Player* actor, Monster* target, string playerClass)
{
	mDamage = (mPow - (actor->pVit));
	if (mMonsterClass == "Warrior") // warrior strong against assassin
	{
		if (playerClass == "Assassin") mDamage = (mPow - (actor->pVit)) * 2;
		else mDamage = (mPow - (actor->pVit));
	}
	else if (mMonsterClass == "Assassin") // assassin strong against mage
	{
		if (playerClass == "Mage")  mDamage = (mPow - (actor->pVit)) * 2;
		else mDamage = (mPow - (actor->pVit));
	}
	else if (mMonsterClass == "Mage") // mage strong against warrior
	{
		if (playerClass == "Warrior")mDamage = (mPow - (actor->pVit)) * 2;
		else mDamage = (mPow - (actor->pVit));
	}

	if (mDamage <= 0)  mDamage = 1;

	cout << mName << " dealt " << mDamage << " to " << actor->mName << "!!!\n";
	actor->takeDamage(mDamage);
}

void Monster::mViewStats()
{
	cout << "Class = " << mMonsterClass << endl
		<< "Hp = " << mHp << "/" << mBaseHp << endl
		<< "Pow = " << mPow << endl
		<< "Vit = " << mVit << endl
		<< "Dex = " << mDex << endl
		<< "Agi = " << mAgi << endl;
}