#include<string>
#include<cstdlib>

using namespace std;

#pragma once
class Monster;

class Player
{
public:
	Player();
	Player(string name, string playerClass, int hp, int baseHp, int pow, int vit, int dex, int agi, int heal);
	~Player();

	int getHp();
	int getBaseHp();
	int getPow();
	int getVit();
	int getDex();
	int getAgi();
	string getName();
	void takeDamage(int damage);
	void jobStats();
	void getStats(string monsterClass);
	void getHeal();
	void selectClass(int playerChoice);
	void pAttack(Player* actor, Monster* target, string monsterClass); //attack here para madali
	void viewStats();

protected:
	int pVit;
	int pDamage;
	friend class Monster;
	friend int main();

private:
	string mName;
	string mPlayerClass;
	int pHp;
	int mBaseHp;
	int mPow;
	int mDex;
	int pAgi;
	int mHeal;
	
};

