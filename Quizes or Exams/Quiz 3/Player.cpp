#include "Player.h"
#include "Monster.h"
#include<iostream>

class Monster;

Player::Player()
{
	mName = "Random Guy";
	mPlayerClass = "Random Job";
	pHp = (rand() % 10 + 1) + 20;
	mBaseHp = pHp;
	mPow = 5;
	pVit = 5;
	mDex = 5;
	pAgi = 5;

	mHeal = mBaseHp * 30 / 100;
}

Player::Player(string name, string playerClass, int hp, int baseHp, int pow, int vit, int dex, int agi, int heal)
{
	mName = name;
	mPlayerClass = playerClass;
	pHp = hp;
	mBaseHp = baseHp;
	mPow = pow;
	pVit = vit;
	mDex = dex;
	pAgi = agi;

	mHeal = heal;
}

Player::~Player() //deconstructor
{
}

int Player::getHp()
{
	return pHp;
}

int Player::getBaseHp()
{
	return mBaseHp;
}

int Player::getPow()
{
	return mPow;
}

int Player::getVit()
{
	return pVit;
}

int Player::getDex()
{
	return mDex;
}

int Player::getAgi()
{
	return pAgi;
}

string Player::getName()
{
	return mName;
}

void Player::takeDamage(int damage)
{
	if (damage < 0) return;
	pHp -= damage;
	if (pHp < 0) pHp = 0;
}

void Player::jobStats()
{
	if (mPlayerClass == "Warrior")
	{
		pHp = pHp + ((rand() % 5 + 1) + 5);
		mBaseHp = pHp;
		pVit = pVit + ((rand() % 4 + 1) + 2);
	}
	else if (mPlayerClass == "Assassin")
	{
		mPow = mPow + ((rand() % 3 + 1) + 2);
		pAgi = pAgi + ((rand() % 4 + 1) + 2);
	}
	else if (mPlayerClass == "Mage")
	{
		pVit = (rand() % 3 + 1) + 1;
		mPow = mPow + ((rand() % 5 + 1) + 5);
		mDex = mDex + ((rand() % 4 + 1) + 2);
	}
}

void Player::getStats(string monsterClass)
{
	if (monsterClass == "Warrior")
	{
		mBaseHp += 3;
		pHp += 3;
		pVit += 3;
	}
	else if (monsterClass == "Assassin")
	{
		pAgi += 3;
		mDex += 3;
	}
	else if (monsterClass == "Mage")
		mPow += 5;
}

void Player::getHeal()
{
	mHeal = (mBaseHp * 3) / 100;
	pHp = pHp + mHeal;
	if (pHp > mBaseHp) pHp = mBaseHp;
}

void Player::selectClass(int playerChoice)
{
	if (playerChoice == 1) mPlayerClass = "Warrior";
	else if (playerChoice == 2) mPlayerClass = "Assassin";
	else if (playerChoice == 3) mPlayerClass = "Mage";
}

void Player::pAttack(Player* actor, Monster* target, string monsterClass)
{
	pDamage = (mPow - (target->mVit)); 
	if (mPlayerClass == "Warrior") // warrior strong against assassin
	{
		if (monsterClass == "Assassin") pDamage = (mPow - (target->mVit)) * 2;
		else pDamage = (mPow - (target->mVit));
	}
	else if (mPlayerClass == "Assassin") // assassin strong against mage
	{
		if (monsterClass == "Mage")  pDamage = (mPow - (target->mVit)) * 2;
		else pDamage = (mPow - (target->mVit));
	}
	else if (mPlayerClass == "Mage") // mage strong against warrior
	{
		if (monsterClass == "Warrior")pDamage = (mPow - (target->mVit)) * 2;
		else pDamage = (mPow - (target->mVit));
	}

	if (pDamage <= 0)  pDamage = 1;

	cout << mName << " dealt " << pDamage << " to " << target->mName << "!!!\n";
	target->takeDamage(pDamage);
}

void Player::viewStats()
{
	cout << "Name = " << mName << endl
		<< "Class = " << mPlayerClass << endl
		<< "Hp = " << pHp << "/" << mBaseHp << endl
		<< "Pow = " << mPow << endl
		<< "Vit = " << pVit << endl
		<< "Dex = " << mDex << endl
		<< "Agi = " << pAgi << endl;
}