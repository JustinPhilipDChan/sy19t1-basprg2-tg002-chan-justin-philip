#include <iostream>
#include <string>
#include <time.h>
#include "Player.h"
#include "Monster.h"

using namespace std;

int main()
{
	srand(time(NULL));
	int playerChoice;

	Player* player = new Player;

	cout << "Insert name: ";
	cin >> player->mName;
	cout << endl;
	system("pause");

	cout << "\n[1] Warrior" << endl << "[2] Assassin" << endl << "[3] Mage";
	cout << endl << "Select a Class: ";
	do {
		cin >> playerChoice;
		player->selectClass(playerChoice);
	}while ((playerChoice >= 4) && (playerChoice <= 0));
	
	player->jobStats();

	do {
		cout << "\n\nPlayer:\n";
		player->viewStats();
		cout << "\n\nMonster:\n";
		Monster* monster = new Monster;
		monster->mJobStats();
		monster->mViewStats();
		system("pause"); system("cls");

		while((monster->mHp) > 0) {
			if ((player->pAgi > monster->mAgi) || (player->pAgi == monster->mAgi)) // player has higher/same agi 
			{
				player->pAttack(player, monster, monster->mMonsterClass);
				system("pause");
				cout << endl << endl;
				monster->mAttack(player, monster, player->mPlayerClass);
				system("pause");
				cout << endl << endl;
			}
			else if (monster->mAgi > player->pAgi) // monster has higher agi
			{
				monster->mAttack(player, monster, player->mPlayerClass);
				system("pause");
				cout << endl << endl;
				player->pAttack(player, monster, monster->mMonsterClass);
				system("pause");
				cout << endl << endl;
			}
		}

		if (monster->mHp <= 0)
		{
			player->getHeal(); //getHeal not working but getStats is working
			player->getStats(monster->mMonsterClass);
			system("pause");
			cout << "Enemy " << monster->mMonsterClass << " has been defeated!\n";
			system("pause"); system("cls");
		}

		delete monster;

		if (player->pHp <= 0)
		{
			break;
		}

	} while ((player->pHp) >= 0);

	if (player->pHp == 0)
	{
		system("pause"); system("cls");
		cout << "Thanks for playing the game, here's your final stat!\n\n";
		player->viewStats();
	}
	delete player;

	system("pause");
	return 0;
}