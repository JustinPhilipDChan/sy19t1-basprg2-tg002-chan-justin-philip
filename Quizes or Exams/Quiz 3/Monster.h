#include<string>
#include<cstdlib>

using namespace std;

#pragma once

class Player;

class Monster
{
public:
	Monster();
	Monster(string name,string monsterClass, int hp, int baseHp, int pow, int vit, int dex, int agi, int random);
	~Monster();

	int getHp();
	int getBaseHp();
	int getPow();
	int getVit();
	int getDex();
	int getAgi();
	void takeDamage(int damage);
	void mJobStats();
	void mAttack(Player* actor, Monster* target, string playerClass);
	void mViewStats();

protected:
	int mVit;
	int mDamage;
	friend class Player;
	friend int main();

private:
	string mName;
	string mMonsterClass;
	int mHp;
	int mBaseHp;
	int mPow;
	int mDex;
	int mAgi;
	int mRandom;
	
};

